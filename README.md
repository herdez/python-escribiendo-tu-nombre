### Escribiendo tu nombre en Python

El programa deberá imprimir tu nombre en la pantalla, el programa terminará su ejecución cuando el usuario oprima la tecla `intro`.

Para este ejercicio tendrás que documentarte sobre las instrucciones necesarias que usa Python para:

- 1) Imprimir en pantalla.
- 2) Obtener entradas del usuario.

> Se entregará un programa en python.